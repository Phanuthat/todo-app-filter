import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'filter-categories';
  constructor(private http: HttpClient) { }
  categories: Array<string[]> = []
  searchField = new FormControl();
  searchValue: string = ""


  ngOnInit() {
    this.setCategories()
  }

  ngOnDestroy() {
    this.categories = []
  }

  stringFirstToUpperCase = (props: string) => {
    return props.charAt(0).toUpperCase() + props.slice(1).toLocaleLowerCase()
  }

  filterCatgoriesByName = (data: Array<string[]>, value: string) => {
    return data.filter((item: string[]) => {
      return item.includes(this.stringFirstToUpperCase(value)) && item
    })
  }

  setCategories = () => {
    this.getCategories().subscribe((data: Array<string[]>) => {
      if (!!this.searchValue) {
        const newCategories: Array<string[]> = this.filterCatgoriesByName(data, this.searchValue)
        this.categories = newCategories
      } else {
        this.categories = data
      }
    })
  }

  getCategories = () => {
    const url = 'https://api.publicapis.org/categories'
    return this.http.get(url)
  }


  searchCategory = () => {
    this.searchValue = this.searchField.value;
    this.setCategories()
    this.searchField.reset('');
  }
}
